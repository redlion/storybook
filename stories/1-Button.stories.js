import React from 'react';
import { action } from '@storybook/addon-actions';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';


const useStyles = makeStyles(theme => ({
  root: {
    '& > *': {
      margin: theme.spacing(1)
    },
  },
  margin: {
    margin: theme.spacing(1),
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
}));

export const ContainedButtons = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Button variant="contained">Default</Button>
      <Button variant="contained" color="primary">
        Primary
      </Button>
      <Button variant="contained" color="secondary">
        Secondary
      </Button>
      <Button variant="contained" disabled>
        Disabled
      </Button>
      <Button variant="contained" color="primary" href="#contained-buttons">
        Link
      </Button>
    </div>
  );
}

export const DisableElevation = () =>  {
  return (
    <Button variant="contained" color="primary" disableElevation>
      Disable elevation
    </Button>
  );
}


export const TextButtons = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Button>Default</Button>
      <Button color="primary">Primary</Button>
      <Button color="secondary">Secondary</Button>
      <Button disabled>Disabled</Button>
      <Button href="#text-buttons" color="primary">
        Link
      </Button>
    </div>
  );
}



export const ButtonSizes = () => {
  const classes = useStyles();

  return (
    <div>
      <div>
        <Button onClick={action('clicked')} size="small" className={classes.margin}>
          Small
        </Button>
        <Button onClick={action('clicked')} size="medium" className={classes.margin}>
          Medium
        </Button>
        <Button onClick={action('clicked')} size="large" className={classes.margin}>
          Large
        </Button>
      </div>
      <div>
        <Button onClick={action('clicked')} variant="outlined" size="small" color="primary" className={classes.margin}>
          Small
        </Button>
        <Button onClick={action('clicked')} variant="outlined" size="medium" color="primary" className={classes.margin}>
          Medium
        </Button>
        <Button onClick={action('clicked')} variant="outlined" size="large" color="primary" className={classes.margin}>
          Large
        </Button>
      </div>
      <div>
        <Button onClick={action('clicked')} variant="contained" size="small" color="primary" className={classes.margin}>
          Small
        </Button>
        <Button onClick={action('clicked')} variant="contained" size="medium" color="primary" className={classes.margin}>
          Medium
        </Button>
        <Button onClick={action('clicked')} variant="contained" size="large" color="primary" className={classes.margin}>
          Large
        </Button>
      </div>
      <div>
        <IconButton aria-label="delete" className={classes.margin} size="small">
          <ArrowDownwardIcon fontSize="inherit" />
        </IconButton>
        <IconButton aria-label="delete" className={classes.margin}>
          <DeleteIcon fontSize="small" />
        </IconButton>
        <IconButton aria-label="delete" className={classes.margin}>
          <DeleteIcon />
        </IconButton>
        <IconButton aria-label="delete" className={classes.margin}>
          <DeleteIcon fontSize="large" />
        </IconButton>
      </div>
    </div>
  );
}

export default {
  title: 'Button',
  component: Button,
};

export const Text = () => <Button onClick={action('clicked')}  variant="contained" color="primary">Hello Button</Button>;

export const Emoji = () => (
  <Button onClick={action('clicked')}>
    <span role="img" aria-label="so cool">
      😀 😎 👍 💯
    </span>
  </Button>
);
