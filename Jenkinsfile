node {
    podTemplate(label: 'build-application',
        containers: [
            containerTemplate(name: 'docker', image: 'docker', ttyEnabled: true, command: 'cat'),
            containerTemplate(name: 'helm', image: '10.52.181.240:8000/51scrum/helm:v1.0', ttyEnabled: true, command: 'cat')
        ],
        volumes: [
            hostPathVolume(hostPath: '/var/run/docker.sock', mountPath: '/var/run/docker.sock'),
            hostPathVolume(hostPath: '/home/service/.kube', mountPath: '/home/service/.kube')
        ],
        imagePullSecrets: [
            '51scrum'
        ]
    ) {
        node('build-application') {
            def dockerRegistry = '10.52.181.240:8000/51scrum'
            def kubeConfigPath = '/home/service/.kube/config'
            def git
            def commitHash
            def gitRepoName
            def gitBranchName
            def dockerImageName
            def helmChartName
            def ingressHost


            stage('Checkout') {
                git = checkout scm
                echo "*************************************"
                echo "${git}"
                echo "*************************************"
                commitHash = git.GIT_COMMIT
                gitRepoName = git.GIT_URL.tokenize("/")[3].replaceAll(".git", "")
                gitBranchName = git.GIT_BRANCH.replaceAll("origin/", "");
                dockerImageName = "${gitRepoName}-${gitBranchName.replaceAll('/', '-')}"
                helmChartName = "${gitRepoName}-${gitBranchName.replaceAll('/', '-')}"
                ingressHost = "${gitRepoName}.${gitBranchName.replaceAll('/', '.')}.10.52.181.241.xip.io"
            }

            stage('Docker Image Build') {
                container('docker') {
                    sh "docker build -t ${dockerRegistry}/${dockerImageName}:${gitBranchName.replaceAll('/', '-')}-${env.BUILD_NUMBER} ."
                }
            }
            
            stage('Docker Image Push') {
                container('docker') {
                    docker.withRegistry("http://10.52.181.240:8000/51scrum/${gitRepoName}", 'harbor-admin') {
                        sh "docker push ${dockerRegistry}/${dockerImageName}:${gitBranchName.replaceAll('/', '-')}-${env.BUILD_NUMBER}"
                    }
                }
            }

            stage('Remove Build Docker Image') {
                container('docker') {
                    sleep 5
                    sh "docker rmi ${dockerRegistry}/${dockerImageName}:${gitBranchName.replaceAll('/', '-')}-${env.BUILD_NUMBER}"
                }
            }
            
            stage('Kubernetes Helm Deploy') {
                def servicePort = 80
                def serviceTargetPort = 80
                def deploymentPort = 80

                container('helm') {
                    try{
                        retry(10) {
                            sleep 5
                            sh "helm repo update"
                            sleep 3
                            sh "helm pull chartmuseum/helm-template"
                            sh "tar -xvf helm-template-1.tgz"
                            sh "sed -i 's/helm-template/${helmChartName}/g' ./helm-template/Chart.yaml"
                            sh "sed -i 's/project_description/A helm chart for ${helmChartName}/g' ./helm-template/Chart.yaml"
                            sh "sed -i 's/0.0.0/${env.BUILD_NUMBER}/g' ./helm-template/Chart.yaml"
                            sh "helm push ./helm-template/ --version ${env.BUILD_NUMBER} chartmuseum"
                            sleep 3
                            sh "helm repo update"
                        }
                    } catch (e) {
                        error("Error occured in the helm container");
                    }

                    def helmList = sh script: "helm list --kubeconfig ${kubeConfigPath} -q --namespace default", returnStdout: true
                    
                    if(helmList.contains("${helmChartName}")) {
                        echo "Already installed. Upgrade from helm repository!"
                        sh "helm upgrade ${helmChartName} --kubeconfig ${kubeConfigPath} --set name=${helmChartName},configmap.name=configmap-${helmChartName},deployment.name=deployment-${helmChartName},deployment.port=${deploymentPort},service.name=service-${helmChartName},service.port=${servicePort},service.targetPort=${serviceTargetPort},ingress.name=ingress-${helmChartName},ingress.host=${ingressHost},image.tag=${gitBranchName.replaceAll('/', '-')}-${env.BUILD_NUMBER},version=${env.BUILD_NUMBER} --version ${env.BUILD_NUMBER} chartmuseum/${helmChartName}"
                    }else{
                        echo "Install from helm repository!"
                        sh "helm install ${helmChartName} --kubeconfig ${kubeConfigPath} --set name=${helmChartName},configmap.name=configmap-${helmChartName},deployment.name=deployment-${helmChartName},deployment.port=${deploymentPort},service.name=service-${helmChartName},service.port=${servicePort},service.targetPort=${serviceTargetPort},ingress.name=ingress-${helmChartName},ingress.host=${ingressHost},image.tag=${gitBranchName.replaceAll('/', '-')}-${env.BUILD_NUMBER},version=${env.BUILD_NUMBER} --version ${env.BUILD_NUMBER} chartmuseum/${helmChartName}"
                    }
                }
            }

            stage('Clean Up Environment') {
                container('docker') {
                    sh "docker system prune -f -a"
                }
            }
        }
    }
}
