# build environment
FROM node:12.2.0-alpine as build


# 앱 디렉터리 생성
WORKDIR /usr/src/app

COPY . .

RUN npm install --unsafe-perm

RUN npm run build-storybook

# production environment
FROM nginx:1.16.0-alpine

RUN apk --no-cache add tzdata && \
        cp /usr/share/zoneinfo/Asia/Seoul /etc/localtime && \
        echo "Asia/Seoul" > /etc/timezone


COPY default.conf /etc/nginx/conf.d/default.conf
COPY expires.conf /etc/nginx/conf.d/expires.conf

COPY --from=build /usr/src/app/storybook-static /usr/src/app/storybook-static
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
